import React from "react";
import Card from "../Card/Card";

import styles from "./Post.module.scss";

const Post = (props) => {
  return (
    <Card className={styles.cardStyling}>
      <div onClick={props.onClick}>
        <div>Title - {props.title}</div>
        <div>Author - {props.author}</div>
        <div>Body-{props.body}</div>
      </div>
    </Card>
  );
};

export default Post;
