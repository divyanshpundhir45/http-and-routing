import React, { useEffect, useState } from "react";
import axios from "axios";
import Card from "../Card/Card";

import styles from "./FullPost.module.scss";

const FullPost = (props) => {
  const [postDetails, setpostDetails] = useState(null);
  useEffect(() => {
    const id=props.match.params.id;
    if (id) {
      axios
        .get(`https://jsonplaceholder.typicode.com/posts/${id}`)
        .then((response) => {
          const s = { ...response.data, author: "Divyansh" };
          setpostDetails(s);
        });
    }
  }, []);

  const getEmptyCard = () => (
    <Card className={styles.cardStyling}>
      <div>
        <div className={styles.emptyText}>Title</div>
        <div className={styles.emptyText}>Body</div>
        <div className={styles.emptyText}>Author</div>
      </div>
    </Card>
  );

  const getDisplayCard = () => (
    <Card className={styles.cardStyling}>
      <div>Title - {postDetails.title}</div>
      <div>Author - {postDetails.author}</div>
      <div>Body-{postDetails.body}</div>
    </Card>
  );

  return (
    <>
    <div className={styles.allPostHeading}>Full Post</div>
    <div>{postDetails ? getDisplayCard() : getEmptyCard()}</div>;
    </>
  )
};

export default FullPost;
