import React, { useState} from "react";
import Card from "../Card/Card";
import axios from "axios";

import styles from "./NewPost.module.scss";
import { Redirect } from "react-router-dom";

const NewPost = (props) => {
  const [title, setTitle] = useState("");
  const [body, setBody] = useState("");
  const [author, setAuthor] = useState("");
  const [isSubmitted,setisSubmitted] = useState(false);
  console.log(props);
  const onTitleChange = (event) => {
    setTitle(event.target.value);
  };

  const onAuthorChange = (event) => {
    setAuthor(event.target.value);
  };

  const onBodyChange = (event) => {
    setBody(event.target.value);
  };

  const _onSubmit = (e) => {
    const obj = { title, body, author, id: new Date() };
    axios
      .post("https://jsonplaceholder.typicode.com/posts", obj)
      .then((response) => {
        console.log("response", response);
      });
    setAuthor("");
    setBody("");
    setTitle("");
    setisSubmitted(true);
  };
  return (
    <>
     <div className={styles.allPostHeading}>Add a new Post</div>
    <Card className={styles.cardStyling}>
      <div className={styles.divStyle}>
        Add Title
        <input value={title} onChange={onTitleChange} />
      </div>
      <div className={styles.divStyle}>
        Add Author
        <input value={author} onChange={onAuthorChange} />
      </div>
      <div className={styles.divStyle}>
        Add Body
        <input value={body} onChange={onBodyChange} />
      </div>
      <button type="submit" onClick={_onSubmit}>
        Add Post
      </button>
      { isSubmitted ? <Redirect to='/'/> : null}
    </Card>
    </>
  );
};

export default NewPost;
