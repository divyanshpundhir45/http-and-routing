import React, { useState, useEffect } from "react";
import { Route } from 'react-router-dom';
import axios from "axios";
import Post from "../../Components/Post/Post";
import FullPost from '../../Components/FullPost/FullPost';
import styles from "./Blogs.module.scss";

const Blogs = (props) => {
  const [posts, setPosts] = useState([]);
  useEffect(() => {
    axios.get("https://jsonplaceholder.typicode.com/posts").then((response) => {
      const currPost = response.data.slice(0, 4);
      const updatedPost = currPost.map((p) => ({
        ...p,
        author: "Divyansh",
      }));
      setPosts(updatedPost);
    });
  }, []);

  const _onClick = (id) => {
    props.history.push(`/${id}`);
  };
  return (
    <div>
      <div className={styles.allPostHeading}>All posts</div>
      <div className={styles.postWrapper}>
        {posts.map((post) => (
          <Post
            key={post.id}
            title={post.title}
            body={post.body}
            author={post.author}
            onClick={() => _onClick(post.id)}
          />
        ))}
      </div>
      <Route exact path='/:id' component={FullPost} />
    </div>
  );
};

export default Blogs;
