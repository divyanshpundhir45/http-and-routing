import React from "react";
import Blogs from "./Containers/Blogs/Blogs";
import NewPost from './Components/NewPost/NewPost';
import FullPost from './Components/FullPost/FullPost';
import { BrowserRouter as Router, Route , Switch , Link} from 'react-router-dom';

import styles from './App.module.scss';

function App() {
  return(
    <Router>
       <div className={styles.navwrapper}>
          <div style={{margin : '20px'}}>
            <Link to='/'>
            Posts
            </Link>
          </div>
          <div style={{margin : '20px'}}>
            <Link to='/new-post'>
            New-Post
            </Link>
          </div>
       </div>
        <Switch>
           <Route exact path='/new-post' component={NewPost} />
           <Route path='/' component={Blogs} />
        </Switch>
    </Router>
  );
};

export default App;
